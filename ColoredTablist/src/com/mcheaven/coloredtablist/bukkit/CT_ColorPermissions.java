package com.mcheaven.coloredtablist.bukkit;

import java.util.ArrayList;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class CT_ColorPermissions {
	private final String premsg = ChatColor.GOLD + "[ColoredTablist] "
			+ ChatColor.RED;
	private final ArrayList<String> gotmessage = new ArrayList<String>();

	private boolean setColorsPermission(Player player, String colorname,
			String permission, CT_Main plugin) {
		OfflinePlayer oplayer = Bukkit.getOfflinePlayer(player.getUniqueId());
		String worldname = player.getLocation().getWorld().getName();
		if (plugin.permission.playerHas(worldname, oplayer, permission)) {
			if (permission.equals("coloredtablist.invisible")
					&& !plugin.permission.playerHas(worldname, oplayer,
							"coloredtablist.dontuse.pex")
					&& plugin.invisibilityAllowed) {
				if (!plugin.hiddenplayers.contains(player.getPlayerListName()))
					plugin.hiddenplayers.add(player.getPlayerListName());
				CT_Invisibility.invisiblePlayer(plugin);
				return true;
			}
			if (colorname.length() > 15)
				colorname = colorname.substring(0, 16);
			player.setPlayerListName(colorname);
			plugin.listnames.put(player, colorname);
			return true;
		} else
			return false;
	}

	protected void init(Player player, CT_Main plugin) {
		String playername = player.getName();
		checkEveryPermission(player, plugin);
		if (plugin.ct_config.getUseChatNames())
			playername = player.getDisplayName();
		if (playername.endsWith("(�([a-z0-9]))"))
			playername = playername.substring(0, playername.length() - 2);
		if (setColorsPermission(player, playername, "coloredtablist.invisible",
				plugin)) {
			// Player is invisible
			return;
		} else {
			setColorsPermission(player, ChatColor.AQUA + playername,
					"coloredtablist.aqua", plugin);
			setColorsPermission(player, ChatColor.BLACK + playername,
					"coloredtablist.black", plugin);
			setColorsPermission(player, ChatColor.BLUE + playername,
					"coloredtablist.blue", plugin);
			setColorsPermission(player, ChatColor.BOLD + playername,
					"coloredtablist.bold", plugin);
			setColorsPermission(player, ChatColor.DARK_AQUA + playername,
					"coloredtablist.dark_aqua", plugin);
			setColorsPermission(player, ChatColor.DARK_BLUE + playername,
					"coloredtablist.dark_blue", plugin);
			setColorsPermission(player, ChatColor.DARK_GRAY + playername,
					"coloredtablist.dark_gray", plugin);
			setColorsPermission(player, ChatColor.DARK_GREEN + playername,
					"coloredtablist.dark_green", plugin);
			setColorsPermission(player, ChatColor.DARK_PURPLE + playername,
					"coloredtablist.dark_purple", plugin);
			setColorsPermission(player, ChatColor.DARK_RED + playername,
					"coloredtablist.dark_red", plugin);
			setColorsPermission(player, ChatColor.GOLD + playername,
					"coloredtablist.gold", plugin);
			setColorsPermission(player, ChatColor.GRAY + playername,
					"coloredtablist.gray", plugin);
			setColorsPermission(player, ChatColor.GREEN + playername,
					"coloredtablist.green", plugin);
			setColorsPermission(player, ChatColor.ITALIC + playername,
					"coloredtablist.italic", plugin);
			setColorsPermission(player, ChatColor.LIGHT_PURPLE + playername,
					"coloredtablist.light_purple", plugin);
			setColorsPermission(player, ChatColor.MAGIC + playername,
					"coloredtablist.magic", plugin);
			setColorsPermission(player, ChatColor.RED + playername,
					"coloredtablist.red", plugin);
			setColorsPermission(player, ChatColor.STRIKETHROUGH + playername,
					"coloredtablist.strikethrough", plugin);
			setColorsPermission(player, ChatColor.UNDERLINE + playername,
					"coloredtablist.underline", plugin);
			setColorsPermission(player, ChatColor.WHITE + playername,
					"coloredtablist.white", plugin);
			setColorsPermission(player, ChatColor.YELLOW + playername,
					"coloredtablist.yellow", plugin);
			setColorsPermission(player, plugin.getConfig().getString("prefix1")
					.replaceAll("(&([a-z0-9]))", "�$2")
					+ playername, "coloredtablist.prefix1", plugin);
			setColorsPermission(player, plugin.getConfig().getString("prefix2")
					.replaceAll("(&([a-z0-9]))", "�$2")
					+ playername, "coloredtablist.prefix2", plugin);
			setColorsPermission(player, plugin.getConfig().getString("prefix3")
					.replaceAll("(&([a-z0-9]))", "�$2")
					+ playername, "coloredtablist.prefix3", plugin);
			setColorsPermission(player, plugin.getConfig().getString("prefix4")
					.replaceAll("(&([a-z0-9]))", "�$2")
					+ playername, "coloredtablist.prefix4", plugin);
			setColorsPermission(player, plugin.getConfig().getString("prefix5")
					.replaceAll("(&([a-z0-9]))", "�$2")
					+ playername, "coloredtablist.prefix5", plugin);
			setColorsPermission(player, plugin.getConfig().getString("prefix6")
					.replaceAll("(&([a-z0-9]))", "�$2")
					+ playername, "coloredtablist.prefix6", plugin);
		}
	}

	private void checkEveryPermission(Player player, Plugin plugin) {
		if (player.hasPermission("coloredtablist.dontuse.pex")) {
			plugin.getLogger()
					.log(Level.WARNING,
							player.getName()
									+ " has every Permission. Set UsePermissions to false and add your Groups, instead of using Permission-Nodes!");
			if (!this.gotmessage.contains(player.getName())) {
				player.sendMessage(this.premsg
						+ "Your name in the Tablist is shown incorrectly, contact an admin and tell him to look into the FAQ of ColoredTablist to fix the Problem!");
				this.gotmessage.add(player.getName());
			}
		}
	}

	/*
	 * private void onPlayerScrollName(final Player player, CT_Main plugin,
	 * final String colors) { if
	 * (plugin.scheduledNames.containsKey(player.getName())) { return; } int pid
	 * = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new
	 * Runnable() { Integer x; Integer z; final String nameplus; final String
	 * fullname;
	 * 
	 * public void run() { String secframe = null; String firstframe =
	 * this.nameplus.substring((this.x = Integer.valueOf(this.x.intValue() +
	 * 1)).intValue(), (this.z = Integer.valueOf(this.z.intValue() +
	 * 1)).intValue() + (14 - colors.length())); secframe = colors + firstframe;
	 * if (secframe.contains("+")) { secframe = this.fullname.substring(0, 16);
	 * this.x = Integer.valueOf(-1); this.z = Integer.valueOf(1); }
	 * player.setPlayerListName(secframe); } } , 0L, 15L);
	 * plugin.scheduledNames.put(player.getName(), Integer.valueOf(pid)); }
	 * 
	 * private void onPlayerSwitchAllColors(final Player player, CT_Main plugin,
	 * final String name) { if
	 * (plugin.scheduledNames.containsKey(player.getName())) { return; } int pid
	 * = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new
	 * Runnable() { Integer x = Integer.valueOf(-1);
	 * 
	 * public void run() { ChatColor color = ChatColor.WHITE; if
	 * (this.x.intValue() == 1) color = ChatColor.AQUA; if (this.x.intValue() ==
	 * 2) color = ChatColor.BLACK; if (this.x.intValue() == 3) color =
	 * ChatColor.BLUE; if (this.x.intValue() == 4) color = ChatColor.DARK_AQUA;
	 * if (this.x.intValue() == 5) color = ChatColor.DARK_BLUE; if
	 * (this.x.intValue() == 6) color = ChatColor.DARK_GRAY; if
	 * (this.x.intValue() == 7) color = ChatColor.DARK_GREEN; if
	 * (this.x.intValue() == 8) color = ChatColor.DARK_PURPLE; if
	 * (this.x.intValue() == 9) color = ChatColor.DARK_RED; if
	 * (this.x.intValue() == 10) color = ChatColor.GOLD; if (this.x.intValue()
	 * == 11) color = ChatColor.GRAY; if (this.x.intValue() == 12) color =
	 * ChatColor.GREEN; if (this.x.intValue() == 13) color =
	 * ChatColor.LIGHT_PURPLE; if (this.x.intValue() == 14) color =
	 * ChatColor.RED; if (this.x.intValue() == 15) color = ChatColor.YELLOW; if
	 * (this.x.intValue() > 15) this.x = Integer.valueOf(-1); this.x =
	 * Integer.valueOf(this.x.intValue() + 1); player.setPlayerListName(color +
	 * name); } } , 20L, 160L); plugin.scheduledNames.put(player.getName(),
	 * Integer.valueOf(pid)); }
	 */

}