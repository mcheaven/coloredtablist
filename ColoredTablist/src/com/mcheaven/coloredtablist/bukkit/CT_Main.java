package com.mcheaven.coloredtablist.bukkit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
//import java.util.UUID;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class CT_Main extends JavaPlugin {
	
	private String buildVersion = "1.7.9-R0.2"; //Change when updating imports in CT_Invisibility
	
	public Permission permission = null;

	public CT_ColorPermissions colorPermissions = null;
	public CT_ColorGroups colorgroups = null;

	protected HashMap<Player, Integer> scheduledNames = new HashMap<Player, Integer>();
	protected HashMap<Player, String> listnames = new HashMap<Player, String>();
	protected ArrayList<String> hiddenplayers = new ArrayList<String>();

	protected ArrayList<String> nicknames = new ArrayList<String>();


	private final CT_Listener ct_l = new CT_Listener(this);
	private CT_Commands CT_C;
	public CT_Configuration ct_config;
	public boolean invisibilityAllowed = false;
	

	public void onEnable() {
		Server server = getServer();
		if(server.getBukkitVersion().equals(buildVersion))
			invisibilityAllowed = true;
		else
			server.getLogger().log(Level.INFO, "Server not running on "+buildVersion+". Disabling invisibility Feature!");
		PluginManager pm = server.getPluginManager();
		pm.registerEvents(this.ct_l, this);
		CT_C = new CT_Commands(this);
		getCommand("coloredtablist").setExecutor(this.CT_C);
		colorPermissions = new CT_ColorPermissions();
		colorgroups = new CT_ColorGroups();
		ct_config = new CT_Configuration(this);
		setupPermissions();
		ct_config.createConfig();
		startMetrics();
	}

	public void onDisable() {
	}

	private boolean setupPermissions() {
		RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(Permission.class);
		if (permissionProvider != null) {
			this.permission = ((Permission)permissionProvider.getProvider());
		}
		return this.permission != null;
	}
	
	private void startMetrics() {
		try {
		    MetricsLite metrics = new MetricsLite(this);
		    metrics.start();
		} catch (IOException e) {
		    // Failed to submit the stats :-(
		}
	}
}