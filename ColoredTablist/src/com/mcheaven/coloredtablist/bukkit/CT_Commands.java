package com.mcheaven.coloredtablist.bukkit;

import java.util.Collection;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CT_Commands implements CommandExecutor {

	private final CT_Main plugin;
	private final String premsg = ChatColor.DARK_RED + "[" + ChatColor.GOLD
			+ "ColoredTablist" + ChatColor.DARK_RED + "] " + ChatColor.RED;
	private final String conscmds = premsg + "Console commands: /ctl reload";

	public CT_Commands(CT_Main instance) {
		this.plugin = instance;
	}

	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {

		if (args.length < 1) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(conscmds);
				return true;
			} else
				return false;
		}

		if (!(sender instanceof Player)
				&& !(args[0].equalsIgnoreCase("reload"))) {
			sender.sendMessage(conscmds);
			return true;
		}

		Player player = null;
		Collection<? extends Player> onlinePlayers = plugin.getServer().getOnlinePlayers();
		if (!(args[0].equalsIgnoreCase("reload"))) {
			player = (Player) sender;
		}

		if (args[0].equalsIgnoreCase("reload")) {
			plugin.ct_config.reloadConfig();
			plugin.listnames.clear();
			plugin.nicknames.clear();
			if (plugin.ct_config.getUsePermissions())
				usePermissions(onlinePlayers);
			else {
				useGroups(onlinePlayers);
			}
			sender.sendMessage(this.premsg + "Reloaded!");
			return true;
		}

		if (args[0].equalsIgnoreCase("nick")) {
			if (args.length < 2) {
				String listname = plugin.listnames.get(player);
				player.setPlayerListName(listname);
				sender.sendMessage(premsg + "Listname reset to " + listname);
				return true;
			}
			if (args[1].length() > 16) {
				sender.sendMessage(this.premsg
						+ "Name can't be longer than 16 Characters!");
				return true;
			}
			for (Player oplayer : onlinePlayers)
				if (oplayer.getPlayerListName().equalsIgnoreCase(
						args[1].replaceAll("(&([a-z0-9]))", "�$2"))) {
					sender.sendMessage(this.premsg
							+ "Chosen name already exists in the Tablist!");
					return true;
				}
			((Player) sender).setPlayerListName(args[1].replaceAll(
					"(&([a-z0-9]))", "�$2"));
			sender.sendMessage(premsg + "Listname changed to "
					+ ChatColor.RESET
					+ args[1].replaceAll("(&([a-z0-9]))", "�$2"));
			plugin.nicknames.add(player.getName());
			return true;
		}

		if (args[0].equalsIgnoreCase("help")) {
			sender.sendMessage("-----------------------------------------------------");
			sender.sendMessage("/ctl help " + ChatColor.GOLD + "= "
					+ ChatColor.YELLOW + "Shows this.");
			sender.sendMessage("/ctl reload " + ChatColor.GOLD + "= "
					+ ChatColor.YELLOW + "Reloads the plugin.");
			sender.sendMessage("/ctl colors " + ChatColor.GOLD + "= "
					+ ChatColor.YELLOW + "Shows available Color-Codes.");
			sender.sendMessage("/ctl nick <nickname> " + ChatColor.GOLD + "= "
					+ ChatColor.YELLOW + "Changes name in List.");
			sender.sendMessage("/ctl set usepermissions <true/false> "
					+ ChatColor.GOLD + "= " + ChatColor.YELLOW
					+ "If Permissions should be used.");
			sender.sendMessage("/ctl set usechatnames <true/false>"
					+ ChatColor.GOLD + "= " + ChatColor.YELLOW
					+ "If Displaynames should be used.");
			sender.sendMessage("/ctl set prefix1 <prefix>" + ChatColor.GOLD
					+ "= " + ChatColor.YELLOW
					+ "Sets the prefix for prefix<1>.");
			sender.sendMessage("/ctl set groups <group> <prefix/suffix> <value>"
					+ ChatColor.GOLD + "= " + ChatColor.YELLOW
					+ "Sets the prefix/suffix for <group>.");
			sender.sendMessage("-----------------------------------------------------");
			return true;
		}

		if (args[0].equalsIgnoreCase("set")) {
			if (args.length <= 2) {
				sender.sendMessage(premsg + "Not enough Arguments!");
				return true;
			}
			if (args[1].substring(0, args[1].length() - 1).equalsIgnoreCase(
					"prefix")) {
				if (!plugin.ct_config.changeConfig(args[1], args[2])) {
					sender.sendMessage(premsg + "/ctl set prefix[1-6] "
							+ ChatColor.DARK_BLUE + "Value");
					return true;
				}
				sender.sendMessage(premsg + args[1] + " set to "
						+ ChatColor.RESET + args[2]);
				return true;
			} else if (args[1].equalsIgnoreCase("Groups")) {
				try {
					if (args.length != 5) {
						sender.sendMessage(premsg + "To many/few Arguments!");
						sender.sendMessage("/ctl set groups <group> <prefix/suffix> <value>");
						return true;
					}
					if (args[4].length() > 16) {
						sender.sendMessage(premsg
								+ "Prefix/Suffix value can't be that long!");
						sender.sendMessage("/ctl set groups <group> <prefix/suffix> <value>");
						return true;
					}
					if (!plugin.ct_config.changeConfigGroup(args[2], args[3],
							args[4])) {
						sender.sendMessage(premsg + "Something went wrong..");
						sender.sendMessage("/ctl set groups <group> <prefix/suffix> <value>");
						return true;
					}
					sender.sendMessage(premsg + "Group " + ChatColor.AQUA
							+ args[2] + ChatColor.RED + " set to: ");
					String prefix = plugin.ct_config.getPrefix(args[2]);
					if (plugin.ct_config.getClearPrefix(args[2]).matches(
							"(&([a-z0-9]))+"))
						prefix = prefix
								+ plugin.ct_config.getClearPrefix(args[2]);
					sender.sendMessage("prefix: " + prefix);
					sender.sendMessage("suffix: "
							+ plugin.ct_config.getSuffix(args[2]));
				} catch (IndexOutOfBoundsException e) {
					sender.sendMessage(premsg + "Something went wrong..");
					sender.sendMessage("/ctl set groups <group> <prefix/suffix> <value>");
				}
				return true;
			} else {
				try {
					if (!(args[2].equalsIgnoreCase("true") || args[2]
							.equalsIgnoreCase("false"))) {
						sender.sendMessage(premsg
								+ "/ctl set <key> <true/false>");
						return true;
					}
					if (!plugin.ct_config.changeConfig(args[1],
							Boolean.parseBoolean(args[2]))) {
						sender.sendMessage(premsg + "Sorry the key: "
								+ ChatColor.RESET + ChatColor.BOLD + args[1]
								+ ChatColor.RESET + ChatColor.RED
								+ " doesn't exist in config!");
						sender.sendMessage(premsg
								+ "Possible True/False keys are: "
								+ ChatColor.RESET + "UsePermissions "
								+ "UpdateEvery15Seconds " + ChatColor.RED
								+ "and " + ChatColor.RESET + "UseChatNames");
						return true;
					}
				} catch (Exception e) {
					sender.sendMessage("/coloredtablist set <key> <true/false>");
					return true;
				}
				sender.sendMessage(premsg + args[1] + " set to "
						+ ChatColor.RESET + args[2]);
				return true; // WIP
			}
		}
		if ((args[0].equalsIgnoreCase("colors"))
				|| (args[0].equalsIgnoreCase("color"))
				|| (args[0].equalsIgnoreCase("colours"))
				|| (args[0].equalsIgnoreCase("colour"))) {
			sender.sendMessage("a = " + ChatColor.getByChar("a") + "Result"
					+ ChatColor.RESET + "   |  " + "0 = "
					+ ChatColor.getByChar("0") + "Result");
			sender.sendMessage("b = " + ChatColor.getByChar("b") + "Result"
					+ ChatColor.RESET + "   |  " + "1 = "
					+ ChatColor.getByChar("1") + "Result");
			sender.sendMessage("c = " + ChatColor.getByChar("c") + "Result"
					+ ChatColor.RESET + "   |  " + "2 = "
					+ ChatColor.getByChar("2") + "Result");
			sender.sendMessage("d = " + ChatColor.getByChar("d") + "Result"
					+ ChatColor.RESET + "   |  " + "3 = "
					+ ChatColor.getByChar("3") + "Result");
			sender.sendMessage("e = " + ChatColor.getByChar("e") + "Result"
					+ ChatColor.RESET + "   |  " + "4 = "
					+ ChatColor.getByChar("4") + "Result");
			sender.sendMessage("f = " + ChatColor.getByChar("f") + "Result"
					+ ChatColor.RESET + "   |  " + "5 = "
					+ ChatColor.getByChar("5") + "Result");
			sender.sendMessage("k = " + ChatColor.getByChar("k") + "Result"
					+ ChatColor.RESET + "   |  " + "6 = "
					+ ChatColor.getByChar("6") + "Result");
			sender.sendMessage("l = " + ChatColor.getByChar("l") + "Result"
					+ ChatColor.RESET + "  |  " + "7 = "
					+ ChatColor.getByChar("7") + "Result");
			sender.sendMessage("m = " + ChatColor.getByChar("m") + "Result"
					+ ChatColor.RESET + "   |  " + "8 = "
					+ ChatColor.getByChar("8") + "Result");
			sender.sendMessage("n = " + ChatColor.getByChar("n") + "Result"
					+ ChatColor.RESET + "   |  " + "9 = "
					+ ChatColor.getByChar("9") + "Result");
			sender.sendMessage("o = " + ChatColor.getByChar("o") + "Result");
			return true;
		}
		sender.sendMessage(premsg + "You wrote something wrong..");
		return false;
	}

	private void usePermissions(Collection<? extends Player> onlinePlayers) {
		for (Player player : onlinePlayers)
			this.plugin.colorPermissions.init(player, this.plugin);
		if (plugin.invisibilityAllowed)
			CT_Invisibility.invisiblePlayer(plugin);
	}

	private void useGroups(Collection<? extends Player> onlinePlayers) {
		Logger logger = this.plugin.getLogger();
		boolean useChatNames = plugin.ct_config.getUseChatNames();
		for (Player player : onlinePlayers) {
			String playername = player.getName();
			if (useChatNames)
				playername = player.getDisplayName();
			if (playername.endsWith("(�([a-z0-9]))"))
				playername = playername.substring(0, playername.length() - 2);
			plugin.colorgroups.init(player, plugin, logger, playername);
		}
	}
}