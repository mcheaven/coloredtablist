package com.mcheaven.coloredtablist.sponge;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.tab.TabListEntry;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import com.google.inject.Inject;
import com.mcheaven.coloredtablist.sponge.commands.Command;
import com.mcheaven.coloredtablist.sponge.commands.Footer;
import com.mcheaven.coloredtablist.sponge.commands.Header;
import com.mcheaven.coloredtablist.sponge.commands.Nickname;
import com.mcheaven.coloredtablist.sponge.commands.Reload;

@Plugin(id = "coloredtablist", name = "ColoredTablist", version = "0.93SPO", authors = "McHeaven", description = "The Plugin to color your Tablist!")
public class ColoredTablist {
	private final Text noPermissionsMessage = formatToText("&cNot permitted to use!");
	private final Text chatPrefix = formatToText("&4[&6CTL&4]&r ");

	private TablistListener tablistListener;
	private Configuration config;
	@Inject
	private Logger logger;
	@Inject
	@DefaultConfig(sharedRoot = true)
	private Path configPath;

	private HashMap<UUID, ColoredTablistData> coloredTablistDatas = new HashMap<UUID, ColoredTablistData>();

	@Listener
	public void onServerStart(GameStartedServerEvent event) {
		tablistListener = new TablistListener(this);
		config = new Configuration(this);
		Sponge.getEventManager().registerListeners(this, tablistListener);
		config.loadConfig(configPath);
		setupCommands();
		startMetrics();
	}

	private void setupCommands() {
		Map<List<String>, CommandSpec> children = new HashMap<List<String>, CommandSpec>();
		// TODO Add new Command-Classes here, dont remove this line
		Nickname nickCMD = new Nickname(this);
		Header headerCMD = new Header(this);
		Footer footerCMD = new Footer(this);
		Reload reloadCMD = new Reload(this);

		children.put(nickCMD.getAliases(), nickCMD.getCommand());
		children.put(headerCMD.getAliases(), headerCMD.getCommand());
		children.put(footerCMD.getAliases(), footerCMD.getCommand());
		children.put(reloadCMD.getAliases(), reloadCMD.getCommand());
		Command.buildParentCommand(children);
		Command.register(Sponge.getCommandManager());
	}

	/** Updates Players Header and Footer from Config **/
	public void updateTabHeaderFooter(Player player) {
		updateTabHeaderFooter(config.getColoredTablistData(player), player);
	}

	/** Updates Players Header and Footer **/
	public void updateTabHeaderFooter(ColoredTablistData ctlData, Player player) {
		coloredTablistDatas.put(player.getUniqueId(), ctlData);
		ctlData.setPlayersHeaderAndFooter(player);
	}

	/**
	 * Updates/Adds colored Player to Tablist (updates Tlist of every player)
	 */
	public void updateTabName(Player player) {
		updateTabName(config.getColoredTablistData(player),
				player.getUniqueId());
	}

	/**
	 * Updates/Adds colored Player to Tablist (updates Tlist of every player)
	 */
	public void updateTabName(ColoredTablistData coloredTablistdata, UUID pUUID) {
		Text coloredEntry = coloredTablistdata.getColoredEntry();
		coloredTablistDatas.put(pUUID, coloredTablistdata);
		for (Player lPlayer : Sponge.getServer().getOnlinePlayers()) {
			Optional<TabListEntry> lPTabEntry = lPlayer.getTabList().getEntry(
					pUUID);
			if (lPTabEntry.isPresent())
				lPTabEntry.get().setDisplayName(coloredEntry);
		}
	}

	/**
	 * Removes Player from (intern) Tablist. Use when Player disappears.
	 */
	public void removeFromTablist(Player player) {
		UUID pUUID = player.getUniqueId();
		if (coloredTablistDatas.containsKey(pUUID))
			coloredTablistDatas.remove(pUUID);
	}

	/** Change TablistName (temp) to newName, loads from config if null **/
	public void changeTabName(Player player, String newName) {
		UUID pUUID = player.getUniqueId();
		ColoredTablistData ctlData = coloredTablistDatas.getOrDefault(pUUID,
				config.getColoredTablistData(player));
		if (newName != null)
			ctlData.setName(newName);
		else
			ctlData.setName(config.getColoredTablistData(player).getName());
		updateTabName(ctlData, pUUID);
	}

	/**
	 * Changes Header to newHeader if not null. Returns the Header (from Config
	 * if null)
	 **/
	public Text changeTabHeader(Player player, String newHeader) {
		ColoredTablistData ctlData = coloredTablistDatas.getOrDefault(
				player.getUniqueId(), config.getColoredTablistData(player));
		if (newHeader == null)
			ctlData.setHeader(config.getColoredTablistData(player).getHeader());
		else if(!newHeader.isEmpty())
			ctlData.setHeader(newHeader);
		updateTabHeaderFooter(ctlData, player);
		return ctlData.getHeader();
	}

	/**
	 * Changes Footer to newFooter if not null. Returns the Footer (from Config
	 * if null)
	 **/
	public Text changeTabFooter(Player player, String newFooter) {
		ColoredTablistData ctlData = coloredTablistDatas.getOrDefault(
				player.getUniqueId(), config.getColoredTablistData(player));
		if (newFooter == null)
			ctlData.setFooter(config.getColoredTablistData(player).getFooter());
		else
			ctlData.setFooter(newFooter);
		updateTabHeaderFooter(ctlData, player);
		return ctlData.getFooter();
	}

	/** Reloads/Resets every players TablistEntry **/
	public void reloadTablist() {
		coloredTablistDatas = new HashMap<UUID, ColoredTablistData>();
		for (Player lPlayer : Sponge.getServer().getOnlinePlayers()) {
			updateTabHeaderFooter(lPlayer);
			updateTabName(lPlayer);
		}
	}

	/** Formats a String to become colored Text **/
	public static Text formatToText(String s) {
		return TextSerializers.FORMATTING_CODE.deserialize(s);
	}

	public Logger getLogger() {
		return logger;
	}

	public Text getVersion() {
		return Text.of(Sponge.getPluginManager().fromInstance(this).get()
				.getVersion().get());
	}

	public Text getDescription() {
		return Text.of(Sponge.getPluginManager().fromInstance(this).get()
				.getDescription().get());
	}

	public Text getChatPrefix() {
		return chatPrefix;
	}

	public Configuration getConfig() {
		return config;
	}

	/** Reloads the Config **/
	public void reloadConfig() {
		config.loadConfig(configPath);
	}

	private void startMetrics() {
		try {
			MetricsLite metrics = new MetricsLite(Sponge.getGame(), Sponge
					.getPluginManager().fromInstance(this).get());
			metrics.start();
		} catch (IOException e) {
			logger.debug("Metrics failed to upload.");
		}
	}

	public Text getNoPermissionMessage() {
		return getChatPrefix().concat(noPermissionsMessage);
	}

}
