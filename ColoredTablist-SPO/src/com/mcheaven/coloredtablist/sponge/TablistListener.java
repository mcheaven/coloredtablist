package com.mcheaven.coloredtablist.sponge;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;

public class TablistListener {

	private ColoredTablist coloredTablist;
	
	public TablistListener(ColoredTablist coloredTablist) {
		this.coloredTablist = coloredTablist;
	}

	@Listener
	public void onPlayerJoin(ClientConnectionEvent.Join event) {
		Player player = event.getTargetEntity();
		coloredTablist.updateTabHeaderFooter(player);
		coloredTablist.updateTabName(player);
	}
	
	@Listener
	public void onPlayerDisconnect(ClientConnectionEvent.Disconnect event) {
		Player player = event.getTargetEntity();
		coloredTablist.removeFromTablist(player);
	}
}
