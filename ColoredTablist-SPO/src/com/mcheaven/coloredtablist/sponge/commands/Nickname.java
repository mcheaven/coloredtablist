package com.mcheaven.coloredtablist.sponge.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import com.mcheaven.coloredtablist.sponge.ColoredTablist;

public class Nickname extends Command {
	private static final String _permission = "coloredtablist.command.nickname";
	private static final Text _description = Text
			.of("Temporarily change Tablist-Name of a player");
	private static final List<String> _aliases = Arrays.asList("nick",
			"nickname", "n");

	public Nickname(ColoredTablist plugin) {
		super(plugin, _permission, _description, _aliases);
	}

	@Override
	protected void buildCommand() {
		command = CommandSpec
				.builder()
				.permission(permission)
				.description(description)
				.arguments(
						GenericArguments.optional(GenericArguments.string(Text.of("newName"))),
						GenericArguments.onlyOne(GenericArguments.playerOrSource(Text.of("targetPlayer"))))
				.executor(this).build();
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args)
			throws CommandException {
		Optional<Player> target = args.<Player> getOne("targetPlayer");
		if (!target.isPresent())
			return CommandResult.empty();
		if (target.get().equals(src))
			args.checkPermission(src, permission + ".self");
		String newName = args.<String> getOne("newName").orElse(target.get().getName());
		plugin.changeTabName(target.get(), newName);
		src.sendMessage(plugin.getChatPrefix().concat(
				Text.of("Changed Tablist-Name of '" + target.get().getName()+ "' to: ").concat(ColoredTablist.formatToText(newName))));
		return CommandResult.success();
	}
}
