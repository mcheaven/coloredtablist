package com.mcheaven.coloredtablist.sponge;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class ColoredTablistData {
	
	
	private Text header;
	private Text footer;
	private String prefix;
	private String suffix;
	private String name;
	private Text coloredEntry;

	
	protected ColoredTablistData(String coloredEntry) {
		this.coloredEntry = ColoredTablist.formatToText(coloredEntry);
	}
	
	protected ColoredTablistData(String header, String footer, String prefix, String name, String suffix) {
		if(header != null)
			this.header = ColoredTablist.formatToText(header);
		if(footer != null)
			this.footer = ColoredTablist.formatToText(footer);
		this.prefix = prefix;
		this.name = name;
		this.suffix = suffix;
		coloredEntry = ColoredTablist.formatToText(prefix+name);
		if(suffix != null)
			coloredEntry = coloredEntry.concat(ColoredTablist.formatToText(suffix));
	}
	
	protected Text getColoredEntry() {
		return coloredEntry;
	}
	/** Sets Header and Footer of player, if just one is set the other becomes a blank line **/
	protected void setPlayersHeaderAndFooter(Player player) {
		if(header != null)
			player.getTabList().setHeader(header);
		if(footer != null)
			player.getTabList().setFooter(footer);
	}

	public Text getHeader() {
		return header;
	}

	public Text getFooter() {
		return footer;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getName() {
		return name;
	}

	public void setHeader(String header) {
		if(header != null)
			this.header = ColoredTablist.formatToText(header);
		else
			this.header = null;
	}
	public void setHeader(Text header) {
		this.header = header;
	}

	public void setFooter(String footer) {
		if(footer != null)
			this.footer = ColoredTablist.formatToText(footer);
		else
			this.footer = null;
	}
	public void setFooter(Text footer) {
		this.footer = footer;
	}
	
	public void setPrefixAndSuffix(String prefix, String suffix) {
		this.prefix = prefix;
		this.suffix = suffix;
		coloredEntry = ColoredTablist.formatToText(prefix+name).concat(Text.of(suffix));
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
		coloredEntry = ColoredTablist.formatToText(prefix+name).concat(Text.of(suffix));
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
		coloredEntry = ColoredTablist.formatToText(prefix+name).concat(Text.of(suffix));
	}

	public void setName(String name) {
		this.name = name;
		coloredEntry = ColoredTablist.formatToText(prefix+name).concat(Text.of(suffix));
	}
}
