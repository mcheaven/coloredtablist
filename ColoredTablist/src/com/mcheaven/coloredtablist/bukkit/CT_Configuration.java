package com.mcheaven.coloredtablist.bukkit;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;

public class CT_Configuration {

	private CT_Main plugin;
	
	public CT_Configuration(CT_Main plugin) {
		this.plugin = plugin;
	}
	
	private String configVersion;
	private Boolean updateEvery15Seconds;
	private Boolean usePermissions;
	private Boolean useChatNames;
	private ConfigurationSection groups;
	private String prefix1;
	private String prefix2;
	private String prefix3;
	private String prefix4;
	private String prefix5;
	private String prefix6;
	
	private String[] configList;
	//private final String premsg = "[ColoredTablist]";
	
	
	public void createConfig() {
		Logger logger = plugin.getLogger();
		Configuration config = plugin.getConfig();
		String version = plugin.getDescription().getVersion();
		String configversion = config.getString("ConfigVersion", "bla");
		Boolean usechatnames = true;
		if (!configversion.equals(version)) {
			config.set("ConfigVersion", plugin.getDescription().getVersion());
			if (!config.contains("UpdateEvery15Seconds")) {
				config.set("UpdateEvery15Seconds", Boolean.valueOf(true));
			}
			if (!config.contains("UsePermissions")) {
				config.set("UsePermissions", Boolean.valueOf(true));
			}
			if (config.contains("UseNicknames")) {
				usechatnames = config.getBoolean("UseNicknames");
				config.set("UseNicknames", null);
			}
			if (!config.contains("UseChatNames")) {
				config.set("UseChatNames", usechatnames);
			}
			if (!config.contains("Groups")) {
				config.set("Groups.Member.prefix", "&e");
				config.set("Groups.Admin.prefix", "&a");
				config.set("Groups.Member.suffix", "");
				config.set("Groups.Admin.suffix", "[A]");
			}
			if (config.contains("multi1")) {
				config.set("multi1", null);
			}
			if (config.contains("multi2")) {
				config.set("multi2", null);
			}
			if (config.contains("multi3")) {
				config.set("multi3", null);
			}
			if (config.contains("multi4")) {
				config.set("multi4", null);
			}
			if (config.contains("multi5")) {
				config.set("multi5", null);
			}
			if (config.contains("multi6")) {
				config.set("multi6", null);
			}
			if (!config.contains("prefix1")) {
				config.set("prefix1", "new");
			}
			if (!config.contains("prefix2")) {
				config.set("prefix2", "exp");
			}
			if (!config.contains("prefix3")) {
				config.set("prefix3", "mod");
			}
			if (!config.contains("prefix4")) {
				config.set("prefix4", "admin");
			}
			if (!config.contains("prefix5")) {
				config.set("prefix5", "BOSS");
			}
			if (!config.contains("prefix6")) {
				config.set("prefix6", "idk");
			}

			plugin.saveConfig();
			logger.log(Level.INFO, "Config Updated to Version " + version);
		}
		loadValues();
		logger.log(Level.INFO, "Config loaded");
	}
	
	public void reloadConfig() {
		plugin.reloadConfig();
		loadValues();
	}
	
	private void loadValues() {
		Configuration config = plugin.getConfig();
		this.configVersion = config.getString("ConfigVersion", "0.0.0");
		this.updateEvery15Seconds = config.getBoolean("UpdateEvery15Seconds", true);
		this.usePermissions = config.getBoolean("UsePermissions", true);
		this.useChatNames = config.getBoolean("UseChatNames", true);
		this.groups = config.getConfigurationSection("Groups");
		this.prefix1 = config.getString("prefix1");
		this.prefix2 = config.getString("prefix2");
		this.prefix3 = config.getString("prefix3");
		this.prefix4 = config.getString("prefix4");
		this.prefix5 = config.getString("prefix5");
		this.prefix6 = config.getString("prefix6");
		
		configList = new String[]{ "ConfigVersion", "UpdateEvery15Seconds", "UsePermissions",
				"UseChatNames", "Groups",
				"prefix1", "prefix2", "prefix3", "prefix4", "prefix5", "prefix6"};
	}
	
	
	public void updateOldGroupStuff(String permissionGroup) {
		Configuration config = plugin.getConfig();
		if(config.contains("Groups."+permissionGroup+".Prefix")) {
			config.set("Groups."+permissionGroup+".prefix", config.getString("Groups."+permissionGroup+".Prefix"));
			config.set("Groups."+permissionGroup+".Prefix", null);
		}
		if(config.contains("Groups."+permissionGroup+".Colors"))
			config.set("Groups."+permissionGroup+".Colors", null);
		if(!config.contains("Groups."+permissionGroup+".suffix"))
			config.set("Groups."+permissionGroup+".suffix", "");
		saveConfig(true);
		plugin.getLogger().log(Level.INFO, "Updated Group: "+permissionGroup);
	}
	
	public String getPrefix(String permissionGroup) {
		String prefix = plugin.getConfig().getString("Groups." + permissionGroup + ".prefix");
		if(prefix != null)
			prefix = prefix.replaceAll("(&([a-z0-9]))", "�$2");
		return prefix;
	}
	
	public String getClearPrefix(String permissionGroup) {
		String prefix = plugin.getConfig().getString("Groups." + permissionGroup + ".prefix");
		return prefix;
	}
	
	public String getSuffix(String permissionGroup) {
		String suffix = plugin.getConfig().getString("Groups." + permissionGroup + ".suffix");
		if(suffix != null)
			suffix = suffix.replaceAll("(&([a-z0-9]))", "�$2");
		return suffix;
	}
	
	public boolean changeConfig(String key, boolean value) {
		boolean worked = false;
		for(String realKey : configList)
			if(realKey.equalsIgnoreCase(key)) {
				plugin.getConfig().set(realKey, value);
				plugin.saveConfig();
				worked = true;
				break;
			}
		saveConfig(worked);
		return worked;
	}
	
	public boolean changeConfig(String key, String value) {
		boolean worked = false;
		for(String realKey : configList)
			if(realKey.equalsIgnoreCase(key)) {
				plugin.getConfig().set(realKey, value);
				plugin.saveConfig();
				worked = true;
				break;
			}
		saveConfig(worked);
		return worked;
	}
	
	public boolean changeConfig(String key, String[] value) {
		boolean worked = false;
		for(String realKey : configList)
			if(realKey.equalsIgnoreCase(key)) {
				plugin.getConfig().set(realKey, value);
				plugin.saveConfig();
				worked = true;
				break;
			}
		saveConfig(worked);
		return worked;
	}

	public boolean changeConfigGroup(String keyGroup, String keyValue, String value) {
		boolean worked = false;
		if(!groups.contains(keyGroup))
			groups.createSection(keyGroup);
		if(keyValue.equalsIgnoreCase("prefix") || keyValue.equalsIgnoreCase("suffix")) {
			keyValue = keyValue.toLowerCase();
			groups.getConfigurationSection(keyGroup).set(keyValue, value);
			plugin.saveConfig();
			worked = true;
		}
		saveConfig(worked);
		return worked;
	}
	
	private void saveConfig(boolean save) {
		if(!save)
			return;
		plugin.saveConfig();
		loadValues();
	}

	public String getConfigVersion() {
		return configVersion;
	}

	public Boolean getUpdateEvery15Seconds() {
		return updateEvery15Seconds;
	}

	public Boolean getUsePermissions() {
		return usePermissions;
	}

	public Boolean getUseChatNames() {
		return useChatNames;
	}

	public ConfigurationSection getGroups() {
		return groups;
	}

	public String getPrefix1() {
		return prefix1;
	}

	public String getPrefix2() {
		return prefix2;
	}

	public String getPrefix3() {
		return prefix3;
	}

	public String getPrefix4() {
		return prefix4;
	}

	public String getPrefix5() {
		return prefix5;
	}

	public String getPrefix6() {
		return prefix6;
	}
}
