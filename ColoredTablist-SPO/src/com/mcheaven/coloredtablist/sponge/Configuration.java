package com.mcheaven.coloredtablist.sponge;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.permission.Subject;

import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;

public class Configuration {
	private static final String _header = "header";
	private static final String _footer = "footer";
	private static final String _prefix = "prefix";
	private static final String _suffix = "suffix";
	private static final String _players = "players";
	private static final String _groups = "groups";

	private CommentedConfigurationNode config;
	private CommentedConfigurationNode nodePlayers;
	private CommentedConfigurationNode nodeGroups;

	private ColoredTablist coloredTablist;

	public Configuration(ColoredTablist coloredTablist) {
		this.coloredTablist = coloredTablist;
	}

	public void loadConfig(Path configPath) {
		ConfigurationLoader<CommentedConfigurationNode> loader = HoconConfigurationLoader
				.builder().setPath(configPath).build();
		if (!configPath.toFile().exists()) {
			coloredTablist.getLogger()
					.warn("No Config found. Generating new..");
			generateFromDefault(loader);
		} else
			try {
				config = loader.load();
				coloredTablist.getLogger().info("Config loaded");
			} catch (IOException e) {
				// Won't happen
			}
		loadSingleValues();
	}

	/**
	 * Loads Values which are used often for faster processing
	 */
	private void loadSingleValues() {
		nodePlayers = config.getNode(_players);
		nodeGroups = config.getNode(_groups);
	}

	protected void generateFromDefault(
			ConfigurationLoader<CommentedConfigurationNode> loader) {
		URL jarConfigFile = this.getClass().getResource("defaultConfig.conf");
		ConfigurationLoader<CommentedConfigurationNode> defaultLoader = HoconConfigurationLoader
				.builder().setURL(jarConfigFile).build();
		try {
			config = defaultLoader.load();
			coloredTablist.getLogger().info("Config loaded");
			loader.save(config);
		} catch (IOException e) {
			coloredTablist.getLogger().error("Couldn't save Config!");
		}
	}

	public ColoredTablistData getColoredTablistData(Player player) {
		ColoredTablistData coloredTablistData = null;
		String pUUID = player.getUniqueId().toString();
		String[] tablistDatas = new String[4];
		int i = 0;
		while (i < 4) {
			tablistDatas[i] = nodePlayers.getNode(pUUID, keyOf(i)).getString();
			if (tablistDatas[i] == null) {//Get Group Values
				tablistDatas[i] = getTablistValueFromGroupNodes(player, i);
				if (tablistDatas[i] == null)//Get default Values
					tablistDatas[i] = nodePlayers.getNode(keyOf(i)).getString();
			}
			i++;
		}
		coloredTablist.getLogger().debug("ColoredTablistData: "+tablistDatas[0]+tablistDatas[2]+player.getName()+tablistDatas[3]);
		coloredTablistData = new ColoredTablistData(tablistDatas[0], tablistDatas[1], tablistDatas[2], player.getName(), tablistDatas[3]);
		return coloredTablistData;
	}

	private String getTablistValueFromGroupNodes(Player player, int i) {
		for(Object group : nodeGroups.getChildrenMap().keySet()) {
			for(Subject sGroup : player.getParents()) {
				coloredTablist.getLogger().debug("PLAYER: "+player.getName()+" IDENTIFIER: "+sGroup.getIdentifier()+" Config-Group: "+group);
				if(sGroup.getIdentifier().equalsIgnoreCase(group.toString())) {
					return nodeGroups.getNode(group.toString(), keyOf(i)).getString();
				}
			}
		}
		return null;
	}

	/** Gives a config key (header/footer/prefix/suffix) for a number from 0-3 **/
	protected static String keyOf(int i) {
		if (i == 0)
			return _header;
		else if (i == 1)
			return _footer;
		else if (i == 2)
			return _prefix;
		else return _suffix;
	}

}
