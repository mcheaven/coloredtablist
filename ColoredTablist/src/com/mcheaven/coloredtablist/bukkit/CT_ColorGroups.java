package com.mcheaven.coloredtablist.bukkit;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.entity.Player;

public class CT_ColorGroups {
	
	protected void init(Player player, CT_Main plugin, Logger logger, String playername) {
		String primarygroup = plugin.permission.getPrimaryGroup(player);
		if (primarygroup == null)
			primarygroup = new String("default");
		for (String group : plugin.ct_config.getGroups().getKeys(false))
			if (primarygroup.equalsIgnoreCase(group)) {
				try {
					String prefix = "";
					String suffix = "";
					String fullname = playername;
					if(plugin.ct_config.getPrefix(group) != null)
						prefix = plugin.ct_config.getPrefix(group);
					else {
						plugin.ct_config.updateOldGroupStuff(group);
					}
					if(plugin.ct_config.getSuffix(group) != null)
						suffix = plugin.ct_config.getSuffix(group);
					else
						plugin.ct_config.updateOldGroupStuff(group);
					fullname = prefix + playername + suffix;
					if(fullname.length() > 15) {
						int overlimit = fullname.length() - 16;
						playername = playername.substring(0, playername.length()-overlimit);
					}
					fullname = prefix + playername + suffix;
					player.setPlayerListName(fullname);
				} catch (NullPointerException e) {
					player.setPlayerListName(player.getName());
					logger.log(Level.WARNING, "Please Check Group: " + group + " in Config!");
				}
				plugin.listnames.put(player, player.getPlayerListName());
			}
	}
}