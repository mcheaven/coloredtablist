package com.mcheaven.coloredtablist.sponge.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

import com.mcheaven.coloredtablist.sponge.ColoredTablist;

public abstract class Command implements CommandExecutor {
	private static final String _ppermission = "coloredtablist.command";
	private static final Text _pdescription = Text.of("Base Command. Displays general Infos about this plugin");
	private static final List<String> _paliases = Arrays.asList("coloredtablist", "ctl");
	
	protected final String permission;
	protected final Text description;
	protected final List<String> aliases;
	
	protected static ColoredTablist plugin;
	protected static CommandSpec parentCommand;
	protected CommandSpec command;
	
	protected Command(ColoredTablist plugin, String permission, Text description, List<String> aliases) {
		Command.plugin = plugin;
		this.permission = permission;
		this.description = description;
		this.aliases = aliases;
		buildCommand();
	}
	
	protected void buildCommand() {
		command = CommandSpec.builder().permission(permission).description(description).executor(this).build();
	}
	
	/** Get's the (Child) Command (the actual Command constructed) **/
	public CommandSpec getCommand() {
		return command;
	}

	public static void register(CommandManager cmdManager){
		cmdManager.register(plugin, parentCommand, _paliases);
	}
	
	public static void buildParentCommand(Map<List<String>,? extends CommandCallable> children) {
		parentCommand = CommandSpec.builder().children(children).arguments(GenericArguments.none()).description(_pdescription)/*.permission(_ppermission)*/.executor(new CommandExecutor() {

			@Override
			public CommandResult execute(CommandSource src, CommandContext args)
					throws CommandException {
				src.sendMessage(plugin.getChatPrefix().concat(plugin.getDescription()).concat(Text.of(" Version: ").concat(plugin.getVersion())));
				return CommandResult.success();
			}
			
		}).build();
	}
	
	public String getPermission() {
		return permission;
	}
	
	public Text getDescription() {
		return description;
	}

	public List<String> getAliases() {
		return aliases;
	}

}
