package com.mcheaven.coloredtablist.sponge.commands;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import com.mcheaven.coloredtablist.sponge.ColoredTablist;

public class Footer extends Command {
	private static final String _permission = "coloredtablist.command.footer";
	private static final Text _description = Text
			.of("Sets the Header (of a player)");
	private static final List<String> _aliases = Arrays.asList("footer",
			"foot", "f");

	public Footer(ColoredTablist plugin) {
		super(plugin, _permission, _description, _aliases);
	}

	@Override
	protected void buildCommand() {
		command = CommandSpec
				.builder()
				.permission(permission)
				.description(description)
				.arguments(
						GenericArguments.optional(GenericArguments.string(Text.of("newFooter"))),
						GenericArguments.optional(GenericArguments.player(Text.of("targetPlayer"))))
				.executor(this).build();
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args)
			throws CommandException {
		Collection<Player> targets = args.<Player>getAll("targetPlayer");
		if (targets.isEmpty())
			if(src instanceof Player)
				targets = Arrays.asList((Player)src);
			else {
				src.sendMessage(plugin.getChatPrefix().concat(Text.of("Which Player is your Target?")));
				return CommandResult.empty();
			}
		String newFooter = args.<String> getOne("newFooter").orElse(null);
		String targetNames = new String();
		Text footerResult = null;
		for (Player target : targets) {
			if (target.equals(src))
				args.checkPermission(src, permission + ".self");
			if(targets.size() == 1)
				targetNames = target.getName();
			else
				targetNames = target.getName()+", "+targetNames;
			footerResult = plugin.changeTabFooter(target, newFooter);
		}
		if(footerResult == null)
			src.sendMessage(plugin.getChatPrefix().concat(Text.of("Removed Tablist-Footer of '"+targetNames+"'")));
		else
			src.sendMessage(plugin.getChatPrefix().concat(Text.of("Changed Tablist-Footer of '"+targetNames+"' to: '").concat(footerResult).concat(Text.of("'"))));
		return CommandResult.builder().affectedEntities(targets.size()).build();
	}

}
