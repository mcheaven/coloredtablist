package com.mcheaven.coloredtablist.bukkit;

import java.util.logging.Logger;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class CT_Listener implements Listener {
	private final CT_Main plugin;

	public CT_Listener(CT_Main instance) {
		plugin = instance;
	}

	@EventHandler(priority=EventPriority.NORMAL)
	public void playerChangeWorld(PlayerChangedWorldEvent event) {
		Player player = event.getPlayer();
		if (plugin.ct_config.getUsePermissions())
			usePermissions(player);
		else
			useGroups(player);
	}

	@EventHandler(priority=EventPriority.NORMAL)
	public void playerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (plugin.ct_config.getUsePermissions())
			if(plugin.ct_config.getUpdateEvery15Seconds()) {
				timerUpdateTab(player);
				if(plugin.invisibilityAllowed)
					CT_Invisibility.invisiblePlayer(plugin);
			} else
				delayUpdateTab(player);
		else if(plugin.ct_config.getUpdateEvery15Seconds()) {
			timerUpdateTab(player);	
		} else
			useGroups(player);
	}

	@EventHandler(priority=EventPriority.NORMAL)
	public void playerChat(AsyncPlayerChatEvent event) {
		//TODO Thread safety?
		Player player = event.getPlayer();
		if (plugin.nicknames.contains(player.getName()))
			return;
		if (plugin.ct_config.getUsePermissions()) {
			usePermissions(player);
		}
		else
			useGroups(player);
	}

	@EventHandler(priority=EventPriority.NORMAL)
	public void playerLeave(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (plugin.scheduledNames.containsKey(player)) {
			plugin.getServer().getScheduler().cancelTask(plugin.scheduledNames.get(player));
			plugin.scheduledNames.remove(player);
		}
		if (plugin.nicknames.contains(player.getName()))
			plugin.nicknames.remove(player.getName());
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void playerEssentialsNick(PlayerCommandPreprocessEvent event) {
		String msg = event.getMessage();
		if(msg.length() < 5)
			return;
		msg = msg.substring(0, 5);
		if(!msg.equalsIgnoreCase("/nick"))
			return;
		if(!plugin.ct_config.getUseChatNames())
			return;
		Player player = event.getPlayer();
		if (plugin.ct_config.getUsePermissions()) {
			delayUpdateTab(player);
		}
		else
			delayUpdateTab(player);
	}

	private void usePermissions(Player player) {
		plugin.colorPermissions.init(player, plugin);
		if(plugin.invisibilityAllowed)
			CT_Invisibility.invisiblePlayer(plugin);
	}
	
	private void timerUpdateTab(final Player player) {
		BukkitTask task = new BukkitRunnable() {
        	 
            @Override
            public void run() {
            	if(plugin.nicknames.contains(player.getName()))
            		return;
            	if(plugin.ct_config.getUsePermissions())
            		plugin.colorPermissions.init(player, plugin);
            	else
            		useGroups(player);
            }
 
        }.runTaskTimer(plugin, 10, 300);
        plugin.scheduledNames.put(player, task.getTaskId());
    }

	private void delayUpdateTab(final Player player) {
		
        new BukkitRunnable() {
        	 
            @Override
            public void run() {
            	if(plugin.ct_config.getUsePermissions()) {
            		usePermissions(player);
            	} else
            		useGroups(player);
            }
 
        }.runTaskLater(plugin, 10);
    }


	private void useGroups(Player player) {
		Logger logger = plugin.getLogger();
		String playername = player.getName();
		if(plugin.ct_config.getUseChatNames()) {
			playername = player.getDisplayName();
			if(playername.endsWith("(�([a-z0-9]))"))
				playername = playername.substring(0, playername.length()-2);
			}
		plugin.colorgroups.init(player, plugin, logger, playername);
	}

}