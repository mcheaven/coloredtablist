package com.mcheaven.coloredtablist.sponge.commands;

import java.util.Arrays;
import java.util.List;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.text.Text;

import com.mcheaven.coloredtablist.sponge.ColoredTablist;

public class Reload extends Command {
	private static final String _permission = "coloredtablist.command.reload";
	private static final Text _description = Text
			.of("Reloads the Tablist (and Config)");
	private static final List<String> _aliases = Arrays.asList("reload",
			"rel", "r");

	public Reload(ColoredTablist plugin) {
		super(plugin, _permission, _description, _aliases);
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args)
			throws CommandException {
		plugin.reloadConfig();
		plugin.reloadTablist();
		src.sendMessage(plugin.getChatPrefix().concat(Text.of("Reloaded Config/Tablist")));
		return CommandResult.success();
	}

}
